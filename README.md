# About
These are my solutions to CS61B Fall 2020 discussions.
Below is the course website and list of completed subjects.

<https://fa20.datastructur.es/>

## Discussions
- [X] Disc 01: Intro to Java
- [X] Disc 02: Scope, Pass-by-Value, Static
- [X] Disc 03: Linked Lists, Arrays
- [X] Disc 04: Inheritance, Testing
- [X] Disc 05: Iterators, Iterables
- [X] Disc 06: Disjoint Sets and Asympotics
- [X] Disc 07: More Asympoitcs, Search Trees
- [X] Disc 08: LLRBs, Hashing
- [X] Disc 09: Traversals, Tries, Heaps
- [X] Disc 10: DFS, BFS, Shortest Paths
- [X] Disc 11: Graphs
- [X] Disc 12: Sorting, ADTs
- [ ] Disc 13: More Sorting

## Lectures
- [X] Lecture 01: Intro, Hello World Java
- [X] Lecture 02: Defining and Using Classes
- [X] Lecture 03: References, Recursion, and Lists
- [X] Lecture 04: SLLists, Nested Classes, Sentinel Nodes
- [X] Lecture 05: DLLists, Arrays
- [X] Lecture 06: ALists, Resiziing, vs. SLLists
- [X] Lecture 07: Testing
- [X] Lecture 08: Inheritance, Implements
- [X] Lecture 09: Extends, Casting, Higher Order Functions
- [X] Lecture 10: Subtype Polymorphism vs. HoFs
- [X] Lecture 11: Exceptions, Iterators, Object Methods
- [X] Lecture 12: Asymptotics I
- [X] Lecture 13: Disjoint Sets
- [X] Lecture 14: Asymptotics II
- [X] Lecture 15: ADTs, Sets, Maps, BSTs
- [X] Lecture 16: B-Trees (2-3, 2-3-4 Trees)
- [X] Lecture 17: Red Black Trees
- [X] Lecture 18: Range Searching and Multi-Dimensional Data
- [X] Lecture 19: Hashing
- [X] Lecture 20: Heaps and PQs
- [X] Lecture 21: Prefix Operations and Tries
- [X] Lecture 22: Tree and Graph Traversals
- [X] Lecture 23: Graph Traversals and Implementations
- [X] Lecture 24: Shortest Paths
- [X] Lecture 25: Minimum Spanning Trees
- [X] Lecture 26: Reductions and Decomposition
- [X] Lecture 27: Basic Sorts
- [X] Lecture 28: Quick Sort
- [X] Lecture 29: More Quick Sort, Sorting Summary
- [X] Lecture 30: Sorting and Algorithmic Bounds
- [X] Lecture 31: Radix Sorts
- [ ] Lecture 32: Sorting and Data Structures Conclusion

