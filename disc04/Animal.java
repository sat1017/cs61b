package disc04;

public class Animal {
    /*
    Given the Animal class, fill in the definition of the Cat class so that when greet()
    is called, “Cat [name] says: Meow!” is printed (instead of “Animal [name] says:
    Huh?”). Cats less than the ages of 5 should say “MEOW!” instead of “Meow!”.
    Don’t forget to use @Override if you are writing a function with the same signature
    as a function in the superclass
    */
    protected String name, noise;
    protected int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
        this.noise = "Huh?";
    }

    public String makeNoise() {
        if (age < 5) {
            return noise.toUpperCase();
        } else {
            return noise;
        }
    }

    public void greet() {
        System.out.println("Animal " + name + " says: " + makeNoise());
    }
}

public class Cat extends Animal {
    public Cat(String name, int age) {
        super(name, age);
        this.noise = "Meow!";
    }

    @Override
    public void greet() {
        System.out.println("Cat " + name + " says: " + makeNoise());
    }
}


public class Dog extends Animal {
    /*
    Assume that Animal and Cat are defined as above.
    What would Java print on each of the indicated lines?
    */
    public Dog(String name, int age) {
        super(name, age);
        noise = "Woof!";
    }

    @Override
    public void greet() {
        System.out.println("Dog " + name + " says: " + makeNoise());
    }

    public void playFetch() {
        System.out.println("Fetch, " + name + "!");
    }
}

public class TestAnimals {
    public static void main(String[] args) {
        Animal a = new Animal("Pluto", 10);
        Cat c = new Cat("Garfield", 6);
        Dog d = new Dog("Fido", 4);
        a.greet(); // (A) Animal Pluto says huh?
        c.greet(); // (B) Cat Garfield says Meow!
        d.greet(); // (C) Dog Fido says WOOF!
        a = c; //allowed since cat is a subclass of animal
        ((Cat) a).greet(); // (D) Cat Pluto says Meow!
        a.greet(); // (E) Cat Pluto says Meow!
        a = new Animal("Fluffy", 2);
        c = a; // (F) compile error since c is a cat and a is an animal.
        // an animal is not a cat, but vice versa.
        c = (Cat) a; // (G) run-time error. cannot put a animal into a cat variable
        a = new Dog("Spot", 10); // (H)
        d = a; // (I) compile-time error
        d = (Dog) a; // (J) Dog Spot says Woof!
    }
}