package disc05;
import java.util.Iterator;

/*
Now, define a class OHQueue. We want our OHQueue to be iterable, so that we can
process OHRequest objects with good descriptions. Our constructor will take in an
OHRequest object representing the first request on the queue.
*/
public class OHQueue implements Iterable<OHRequest> {
    OHRequest queue;
    public OHQueue (OHRequest queue) {
        this.queue = queue;
    }

    @Override
    public Iterator<OHRequest> iterator() {
        return new OHRequest(queue);
    }
/*
Fill in the main method below so that you make a new OHQueue object and print
the names of people with good descriptions. Note : the main method is part of the
OHQueue class.
*/
    public static void main(String [] args) {
        OHRequest s5 = new OHRequest("I deleted all of my files", "Alex", null);
        OHRequest s4 = new OHRequest("conceptual: what is Java", "Omar", s5);
        OHRequest s3 = new OHRequest("git: I never did lab 1", "Connor", s4);
        OHRequest s2 = new OHRequest("help", "Hug", s3);
        OHRequest s1 = new OHRequest("no I haven't tried stepping through", "Itai", s2);

        OHQueue q = new OHQueue(s1);
        for (OHRequest i: q) {
            System.out.println(i.name);
        }
    }
}

