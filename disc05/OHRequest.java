package disc05;
/*
1 Iterators Warmup
1. If we were to define a class that implements the interface Iterable<Integer>,
   what method(s) would this class need to define? Write the function signature(s) below.

        public interface Iterable<T> {
                Iterator<T> iterator();
        }


2. If we were to define a class that implements the interface Iterator<Integer>,
   what method(s) would this class need to define? Write the function signature(s) below.

        public interface Iterator<T> {
                boolean hasNext();
                T next();
        }

3. What’s one difference between Iterator and Iterable?
        An Iterable is an object that produce an iterator to iterate over.
*/

/*
The goal for this question is to create an iterable Office Hours queue. We’ll do so
step by step.

The code below for OHRequest represents a single request. Like an IntNode, it has
a reference to the next request. description and name contain the description of
the bug and name of the person on the queue.
*/
public class OHRequest {
        public String description;
        public String name;
        public OHRequest next;

        public OHRequest(String description, String name, OHRequest next) {
                this.description = description;
                this.name = name;
                this.next = next;
        }
}