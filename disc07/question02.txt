Question 2
Give the worst case and best case running time in Θ(·) notation in terms of M and
N. Assume that comeOn() is in Θ(1) and return

Best Case: Θ(N*logM)
Worst Case: Θ(N*M)

for (int i = 0; i < N; i += 1) { //Θ(N)
    for (int j = 1; j <= M; ) {
        if (comeOn()) {         //Θ(1)
            j += 1;             //Θ(M)
        } else {                //Θ(logM)
            j *= 2;
        }
    }
}