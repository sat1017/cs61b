Question 02: Abstract Data Types

For each problem, which of the ADTs given in the previous section might you
use to solve each problem? Which ones will make for a better or more efficient
implementation?

1. Given a news article, find the frequency of each word used in the article.

Map. For every word you encounter for the first time, add the key to the map w/ a value of 1. For every subsequent time you encounter a word, get the value of the key and increment it by 1.

2. Given an unsorted array of integers, return the array sorted from least to
greatest.

PriorityQueue. Assign a value equal to its intenger value. Use a min heap. Delete min and insert into the ith place of an array.

3. Implement the forward and back buttons for a web browser.

Two stacks. One for each button. When you visit a new site, push the previous page onto the back stack. When you go back, push the current page to the forward stack, and pop the back stack to view. When you click forward, push the current page to the back stack and pop the forward stack.
